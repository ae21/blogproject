package de.awacademy.blogproject.user;

import de.awacademy.blogproject.post.PostDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class AdminControler {
    private UserService userService;

    @Autowired
    public AdminControler(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/admin")
    public String admin(Model model, @ModelAttribute("sessionUser") User sessionUser) {
        if (!sessionUser.isAdmin()){
            return "redirect:/";
        }
        List<User> users = userService.getUsers();
        users.remove(sessionUser);
        model.addAttribute("users", users);
        model.addAttribute("newPost", new PostDTO("", ""));
        return "/admin";
    }

    @PostMapping("/admin")
    public String admin(Model model, @RequestParam(value = "usernameToChange") String usernameToChange, @ModelAttribute("sessionUser") User sessionUser){
        if (!sessionUser.isAdmin() || !userService.checkUser(usernameToChange)){
            return "redirect:/";
        }
        User user = userService.getUserByUsername(usernameToChange).get();
        user.setAdmin(!user.isAdmin());
        userService.saveUser(user);
        return "redirect:/admin";
    }
}
