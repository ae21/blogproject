package de.awacademy.blogproject.post;

import de.awacademy.blogproject.comment.Comment;
import de.awacademy.blogproject.comment.CommentDTO;
import de.awacademy.blogproject.comment.CommentService;
import de.awacademy.blogproject.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

@Controller
public class PostController {
    private PostService postService;
    private CommentService commentService;

    @Autowired
    public PostController(PostService postService, CommentService commentService) {
        this.postService = postService;
        this.commentService = commentService;
    }

    @GetMapping("/post/{postId}")
    public String post(@PathVariable int postId, Model model){
        Optional<Post> post = postService.getPostById(postId);
        if (!post.isPresent()){
            return "redirect:/";
        }
        model.addAttribute("post", post.get());
        model.addAttribute("comment", new CommentDTO(""));
        return "post";
    }

    @PostMapping("/post/{postId}")
    public String post(@Valid @ModelAttribute("comment") CommentDTO commentDTO,
                       BindingResult bindingResult, @ModelAttribute("sessionUser")User user,
                       @PathVariable int postId){
        Optional<Post> post = postService.getPostById(postId);
        if (!post.isPresent()){
            return "redirect:/";
        }
        if (bindingResult.hasErrors()){
            return "post";
        }
        Comment comment = new Comment(user, post.get(), commentDTO.getContent(), Instant.now());
        commentService.saveComment(comment);
        return "redirect:/post/"+postId+"/";
    }

    @PostMapping("/newpost")
    public String newpost(@Valid @ModelAttribute("newPost") PostDTO postDTO, BindingResult bindingResult,
                          @ModelAttribute("sessionUser") User sessionUser) {
        if (bindingResult.hasErrors()){
            return "admin";
        }
        Post post = new Post(sessionUser, postDTO.getTitle(), postDTO.getContent(), Instant.now());
        postService.savePost(post);
        return "redirect:/post/" + post.getId() + "/";
    }

    @PostMapping("/deleteComment")
    public String deleteComment(@RequestParam(value = "commentToDelete") Integer commentToDelete,
                                @RequestParam(value = "postID") Integer postID) {
        Optional<Comment> commentOptional = commentService.getCommentById(commentToDelete);
        if (!commentOptional.isPresent()) {
            return "redirect:/post/" + postID.toString() + "/";
        }
        commentService.deleteComment(commentOptional.get());
        return "redirect:/post/" + postID.toString() + "/";
    }

    @PostMapping("/updatePost")
    public String updatePost(Model model, @RequestParam(value = "postToUpdate") Integer postID) {
        Optional<Post> postOptional = postService.getPostById(postID);
        if (!postOptional.isPresent()) {
            return "redirect:/";
        }
        model.addAttribute("postToEdit", new PostDTO(postOptional.get().getTitle(), postOptional.get().getContent()));
        model.addAttribute("postToEditID", postOptional.get().getId());
        return "editPost";
    }

    @PostMapping("/editPost")
    public String editPost(@ModelAttribute("postToEdit") PostDTO postToEditDTO,
                           @RequestParam(value = "postToEditID") Integer postToEditID,
                           @ModelAttribute("sessionUser") User sessionUser) {
        if (!sessionUser.isAdmin()) {
            return "redirect:/";
        }
        Optional<Post> postOptional = postService.getPostById(postToEditID);
        if (!postOptional.isPresent()){
            return "redirect:/";
        }
        Post postToEdit = postOptional.get();
        postToEdit.setTitle(postToEditDTO.getTitle());
        Timestamp timestamp = new Timestamp(Instant.now().toEpochMilli());
        postToEdit.setContent(postToEditDTO.getContent()
                + "\n\"Edited by: " + sessionUser.getUsername() + " @ " +  timestamp + "\"");
        postService.savePost(postToEdit);
        return "redirect:/post/" + postToEdit.getId() + "/";
    }

    @PostMapping("deletePost")
    public String deletePost(@ModelAttribute("sessionUser") User sessionUser,
                             @RequestParam(value = "postToDelete") Integer postToDeleteId) {
        if (!sessionUser.isAdmin()) {
            return "redirect:/";
        }
        Optional<Post> postOptional = postService.getPostById(postToDeleteId);
        if (!postOptional.isPresent()) {
            return "redirect:/";
        }
        for (Comment comment : postOptional.get().getComments()) {
            commentService.deleteComment(comment);
        }
        postService.deletePost(postOptional.get());
        return "redirect:/";
    }

}
