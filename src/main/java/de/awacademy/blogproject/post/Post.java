package de.awacademy.blogproject.post;

import de.awacademy.blogproject.comment.Comment;
import de.awacademy.blogproject.user.User;

import javax.persistence.*;
import java.time.Instant;
import java.util.List;

@Entity
public class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    private User author;

    @OneToMany(mappedBy = "post")
    @OrderBy("postedAt ASC")
    private List<Comment> comments;

    private String title;

    @Column(columnDefinition = "TEXT")
    private String content;
    private Instant postedAt;

    public Post() {}

    public Post(User author, String title, String content, Instant postedAt) {
        this.author = author;
        this.title = title;
        this.content = content;
        this.postedAt = postedAt;
    }

    public int getId() {
        return id;
    }

    public User getAuthor() {
        return author;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public Instant getPostedAt() {
        return postedAt;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setPostedAt(Instant postedAt) {
        this.postedAt = postedAt;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }
}
