package de.awacademy.blogproject.comment;

import de.awacademy.blogproject.post.Post;
import de.awacademy.blogproject.user.User;

import javax.persistence.*;
import java.time.Instant;

@Entity
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    private User author;

    @ManyToOne
    private Post post;

    @Column(columnDefinition = "TEXT")
    private String content;
    private Instant postedAt;

    public Comment() {}

    public Comment(User author, Post post, String content, Instant postedAt) {
        this.author = author;
        this.post = post;
        this.content = content;
        this.postedAt = postedAt;
    }

    public int getId() {
        return id;
    }

    public User getAuthor() {
        return author;
    }

    public Post getPost() {
        return post;
    }

    public String getContent() {
        return content;
    }

    public Instant getPostedAt() {
        return postedAt;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setPostedAt(Instant postedAt) {
        this.postedAt = postedAt;
    }

    public void setPost(Post post) {
        this.post = post;
    }
}
