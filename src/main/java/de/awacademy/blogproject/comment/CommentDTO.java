package de.awacademy.blogproject.comment;

public class CommentDTO {
    private String content;

    public CommentDTO(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }
}
