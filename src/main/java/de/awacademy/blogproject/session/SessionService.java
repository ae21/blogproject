package de.awacademy.blogproject.session;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

@Service
public class SessionService {

    private SessionRepository sessionRepository;

    @Autowired
    public SessionService(SessionRepository sessionRepository) {
        this.sessionRepository = sessionRepository;
    }

    public void saveSesion(Session session){
        if (session.getUser() == null){
            return;
        }
        sessionRepository.save(session);
    }

    public Optional<Session> getSessionByIdNotExpired(String sessionID){
        return sessionRepository.findByIdAndExpiresAtAfter(sessionID, Instant.now());
    }

    public void deleteAllObsoleteSessions() {
        List<Session> sessions = sessionRepository.findAllByExpiresAtBefore(Instant.now());
        for (Session session : sessions) {
            sessionRepository.delete(session);
        }
    }

    public void deleteSession(Session session) {
        sessionRepository.delete(session);
    }
}
