package de.awacademy.blogproject.session;

import de.awacademy.blogproject.user.User;
import de.awacademy.blogproject.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.time.Instant;
import java.util.Optional;

@Controller
public class SessionController {
    private UserService userService;
    private SessionService sessionService;

    @Autowired
    public SessionController(UserService userService, SessionService sessionService){
        this.userService = userService;
        this.sessionService = sessionService;
    }

    @GetMapping("/login")
    public String login(Model model){
        model.addAttribute("login", new LoginDTO("", ""));
        return "login";
    }

    @PostMapping("/login")
    public String login(@ModelAttribute("login") LoginDTO login, BindingResult bindingResult,
                        HttpServletResponse servletResponse){
        Optional<User> user = userService.getUserByUsername(login.getUsername());
        if (!user.isPresent()){
            bindingResult.addError(new FieldError("login", "username", "This user does not exists."));
        }
        else if (!login.getPassword().equals(user.get().getPassword())){
            bindingResult.addError(new FieldError("login", "password", "This password is not correct."));
        }
        if (bindingResult.hasErrors()){
            return "/login";
        }
        Session session = new Session(Instant.now().plusSeconds(60*5), user.get());
        sessionService.saveSesion(session);
        Cookie cookie = new Cookie("sessionID", session.getId());
        servletResponse.addCookie(cookie);
        sessionService.deleteAllObsoleteSessions();
        return "redirect:/";
    }

    @GetMapping("/logout")
    public String logout(@CookieValue(value = "sessionID", defaultValue = "") String sessionId, HttpServletResponse response) {
        Optional<Session> optionalSession = sessionService.getSessionByIdNotExpired(sessionId);
        optionalSession.ifPresent(session -> sessionService.deleteSession(session));

        Cookie cookie = new Cookie("sessionId", "");
        cookie.setMaxAge(0);
        response.addCookie(cookie);

        return "redirect:/";
    }
}
