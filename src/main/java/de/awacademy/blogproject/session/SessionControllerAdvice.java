package de.awacademy.blogproject.session;

import de.awacademy.blogproject.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.time.Instant;
import java.util.Optional;

@ControllerAdvice
public class SessionControllerAdvice {
    private SessionService sessionService;

    @Autowired
    public SessionControllerAdvice(SessionService sessionService) {
        this.sessionService = sessionService;
    }

    @ModelAttribute("sessionUser")
    public User sessionUser(@CookieValue(value = "sessionID", defaultValue = "") String sessionID){
        if (!sessionID.isEmpty()){
            Optional<Session> optionalSession = sessionService.getSessionByIdNotExpired(sessionID);
            optionalSession.ifPresent(session -> {
                session.setExpiresAt(Instant.now().plusSeconds(5*60));
                sessionService.saveSesion(session);
            });
            if (optionalSession.isPresent()){
                Session session = optionalSession.get();
                return session.getUser();
            }

        }
        return null;
    }
}
