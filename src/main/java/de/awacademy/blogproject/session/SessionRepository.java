package de.awacademy.blogproject.session;

import org.springframework.data.repository.CrudRepository;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

public interface SessionRepository extends CrudRepository<Session, String> {
    Optional<Session> findByIdAndExpiresAtAfter(String id, Instant expiresAt);
    List<Session> findAllByExpiresAtBefore(Instant expiresAt);
}
