package de.awacademy.blogproject.session;

import de.awacademy.blogproject.user.User;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.time.Instant;
import java.util.UUID;

@Entity
public class Session {

    @Id
    private String id = UUID.randomUUID().toString();

    private Instant expiresAt;

    @ManyToOne
    private User user;

    public Session() {
    }

    public Session(Instant expiresAt, User user) {
        this.expiresAt = expiresAt;
        this.user = user;
    }

    public String getId() {
        return id;
    }

    public Instant getExpiresAt() {
        return expiresAt;
    }

    public User getUser() {
        return user;
    }

    public void setExpiresAt(Instant expiresAt) {
        this.expiresAt = expiresAt;
    }
}
